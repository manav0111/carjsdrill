// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const AllCarYears = (inventory) => {
  let result = [];

  if (inventory.length > 0) {
    //so we have car data inside inventory so we will loop our inventory array

    for (let index = 0; index < inventory.length; index++) {
      //so here we are pushing all the year's inside our result array
      result.push(inventory[index].car_year);
    }

    //now we are extracting the unique years only
    let uniqueyears = [...new Set(result)];
    return uniqueyears;
  } else {
    return -1;
  }
};

module.exports = AllCarYears;
