// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:

const LastCar= (inventory) => {
  // we will pop last element if the inverntory size is greater than zero
  if (inventory.length > 0) {
    let result = inventory.pop();
    return result;
  } else {
    // it will show that our inventory is empty
    return -1;
  }
};

module.exports = LastCar;
