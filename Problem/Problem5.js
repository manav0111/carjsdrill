// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const CarsBeforeTwoThousand = (inventory) => {
  let result = [];

  if (inventory.length > 0) {
    //we have cars in our inventory

    for (let index = 0; index < inventory.length; index++) {
      //so will loop in car inventory and see all the cars which are before 2000
      if (inventory[index].car_year < 2000) {
        //we will push this car in our result array
        result.push(inventory[index]);
      }
    }

    console.log(result.length);
    return result;
  } else {
    return -1;
  }
};

module.exports = CarsBeforeTwoThousand;
