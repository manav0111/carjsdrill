// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:

const FindtheId = (inventory) => {
  let result;

  // we will find in only if the inventory is not empty
  if (inventory.length > 0) {
    for (let index= 0; index < inventory.length; index++) {
      if (inventory[i].id == 33) {
        result = inventory[i];
        break;
      }
    }

    console.log(result);
    return result;
  } else {
    // if we don't have any element in inventory then we will return -1 shows it is not present
    return -1;
  }
};

module.exports = FindtheId;
