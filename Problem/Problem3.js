// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const SortAlphabetiically = (inventory) => {
  if (inventory.length > 0) {
    // if we have cars inside inventory it will sort them alphabetically
    inventory.sort((first, second) => {
      //it will take everytime two element and sort them according to alphabetically

      let a = first.car_make.toLowerCase();
      let b = second.car_make.toLowerCase();

      if (a < b) {
        return -1;
      }

      if (a > b) {
        return 1;
      }

      return 0;
    });

    return inventory;
    
  } else {
    //our car inventory is empty so we will not perform sorting here
    return -1;
  }
};

module.exports = SortAlphabetiically;