const inventory = require("../Inventory");
const LastCar = require("../Problem/Problem2");

const result = LastCar(inventory);

if (result != -1) {
  //if there is last car is present then we will display the info about the car
  console.log(`Last car is a ${result.car_make} ${result.car_model}`);
} else {
  //if the last car is not there we will display this message
  console.log("inventory is empty there is no car is there in inventory");
}
