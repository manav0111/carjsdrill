const inventory = require("../Inventory");
const AllCarYears = require("../Problem/Problem4");

const result = AllCarYears(inventory);

if (result != -1) {
  //so we will display all the years
  console.log("All the Years in Cars: ");
  console.log(result);
} else {
  console.log("Car inventory is empty");
}
